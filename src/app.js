import React, { Fragment } from 'react';
import Note from './components/Note/Note';

const App = () => {
  return (
    <Fragment>
      <Note />
    </Fragment>
  );
};

export default App;
