import React from 'react';
import ReactDOM from 'react-dom';
import WebFont from 'webfontloader';

import App from './app';

WebFont.load({
  google: {
    families: ['Droid Sans', 'Droid Serif'],
  },
});

import './index.css';

ReactDOM.render(<App />, document.getElementById('root'));
